package am.mediastre.testmediastreamsdkandroid;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.google.android.exoplayer2.ui.PlayerView;

import am.mediastre.mediastreamplatformsdkandroid.MediastreamPlayer;
import am.mediastre.mediastreamplatformsdkandroid.MediastreamPlayerConfig;

public class VODActivity extends AppCompatActivity {
    private PlayerView playerView;
    private MediastreamPlayer mdstrm;
    private MediastreamPlayerConfig config;
    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vod);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        playerView = findViewById(R.id.video_frame);
        container = findViewById(R.id.main_media_frame);
        config = new MediastreamPlayerConfig();
        config.id = "5b5269c08e89904d9ee7f99b";
        config.accountID = "579bd29dc99290cf08362c3b";
        config.environment = MediastreamPlayerConfig.Environment.PRODUCTION;
        config.type = MediastreamPlayerConfig.VideoTypes.VOD;
        config.loop = true;
        config.autoplay = true;
        config.showControls = true;
        mdstrm = new MediastreamPlayer(this, this, playerView, config, container);
    }

    @Override
    public void onBackPressed() {
        mdstrm.releasePlayer();
        super.onBackPressed();
        this.finish();
    }
}

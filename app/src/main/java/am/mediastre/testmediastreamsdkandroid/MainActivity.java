package am.mediastre.testmediastreamsdkandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureButtons();
    }

    private void configureButtons() {
        Button VODButton = (Button) findViewById(R.id.button4);
        VODButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, VODActivity.class));
            }
        });

        Button LIVEButton = (Button) findViewById(R.id.button5);
        LIVEButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, LIVEActivity.class));
            }
        });
    }
}

package am.mediastre.testmediastreamsdkandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.google.android.exoplayer2.ui.PlayerView;

import am.mediastre.mediastreamplatformsdkandroid.MediastreamPlayer;
import am.mediastre.mediastreamplatformsdkandroid.MediastreamPlayerConfig;

public class LIVEActivity extends AppCompatActivity {
    private PlayerView playerView;
    private MediastreamPlayer mdstrm;
    private MediastreamPlayerConfig config;
    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        playerView = findViewById(R.id.video_frame);
        container = findViewById(R.id.main_media_frame);
        config = new MediastreamPlayerConfig();
        config.id = "57b4db886338448314449cfa";
        config.accountID = "579bd29dc99290cf08362c3b";
        config.environment = MediastreamPlayerConfig.Environment.PRODUCTION;
        config.type = MediastreamPlayerConfig.VideoTypes.LIVE;
        config.castAvailable = false;
        config.autoplay = true;
        config.showControls = true;
        mdstrm = new MediastreamPlayer(this, this, playerView, config, container);
    }

    @Override
    public void onBackPressed() {
        mdstrm.releasePlayer();
        super.onBackPressed();
        this.finish();
    }
}
